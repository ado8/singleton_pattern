package com.doerzbach;

public class DbConnection {
    String dbhost,dbname,dbpassword,dbuser,dbport;
    DbConnection(){
        dbname=Main.appConfig.getConfigElement("dbname");
        dbuser=Main.appConfig.getConfigElement("dbuser");
        dbpassword=Main.appConfig.getConfigElement("dbpassword");
        dbhost=Main.appConfig.getConfigElement("dbhost");
        dbport=Main.appConfig.getConfigElement("dbport");
        System.out.printf("Here we would create the Db-Connection with connectstring %s and dbname=%s,dbuser=%s,dbpassword=%s and store it a member variable\n",getConnectString(),dbname,dbuser,dbpassword);
    }

    public String getConnectString() {
        return "jdbc:mysql://"+dbhost+":"+dbport+"/";
    }
}
