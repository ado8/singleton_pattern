package com.doerzbach;

/**
 *  This Example should show how Application Configuration could be implemented as a singleton pattern
*/
public class Main {
    /**
     * This is the Configuration shared by all objects of the application.
     */
    public final static AppConfig appConfig=new AppConfig();
    public static void main(String[] args) {
        DataLoader dataLoader= new DataLoader();
        dataLoader.getdata(new DbConnection());
    }
}
