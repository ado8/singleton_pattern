package com.doerzbach;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This is the class in which the Application Config is stored for access by all classes of the Application
 */
public class AppConfig {
    /**
     * Here the Properties are stored
     */
    private Properties configElements;

    /**
     * Read configuration strings from the resource bundle /application.properties
     */
    public AppConfig() {
        try {
            InputStream in = getClass().getResourceAsStream("/application.properties");
            configElements = new Properties();
            configElements.load(in);

        } catch (IOException ex) {

            // Catch all IOExcoptions and throw a RuntimeException as a Application without exception is not runnable.
            // IOExceptions would have to be catched somewhere RuntimeException not
            throw new RuntimeException("Configuration File not found or readable", ex);
        }
    }

    public String getConfigElement(String key) {
        return configElements.getProperty(key);
    }
}
